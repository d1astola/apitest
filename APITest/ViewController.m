//
//  ViewController.m
//  ReadTwitterRSS
//
//  Created by Tester on 3/15/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "ViewController.h"
#import "json/SBJson5.h"
#import "DrinkCellTableViewCell.h"
#import "DetailedCoctailViewController.h"

@class APITestCoctailsMO;

@interface ViewController ()

@property (assign, nonatomic) NSInteger count;
@property (strong, nonatomic) NSDictionary* dict;
@property (nonatomic, retain) NSMutableData* jsonData;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) APITestCoctailsMO* currentCoctail;
@property (assign, nonatomic) NSInteger index;

- (IBAction)addCoctailAction:(UIBarButtonItem *)sender;
- (IBAction)deleteCoreDataAction:(UIBarButtonItem *)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.count = 0;
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    DetailedCoctailViewController* dcvc = segue.destinationViewController;
    dcvc.coctail = self.currentCoctail;
    NSArray* ingredientsList = [self getAllIngredients:self.currentCoctail];
    dcvc.ingredientsList = ingredientsList;
}

#pragma mark - DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedCoctailsResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DrinkCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    /* search coctail */
    APITestCoctailsMO* coctail = [self.fetchedCoctailsResultsController objectAtIndexPath:indexPath];
    
    /* set label's text */
    cell.nameLabel.text = coctail.name;
    cell.typeLabel.text = coctail.category;
    return cell;
}

#pragma mark - TableViewDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.currentCoctail = [self.fetchedCoctailsResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"showDetailDescription" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

#pragma mark - Private Methods

- (void) insertCoctailInCoreData: (NSDictionary*) dict {
    
    NSManagedObjectContext* context = [self.fetchedCoctailsResultsController managedObjectContext];
    APITestCoctailsMO* coctail = [NSEntityDescription insertNewObjectForEntityForName:@"Coctails"
                                                               inManagedObjectContext:context];
    
    /* set values */
    coctail.identifier = [[dict objectForKey:@"idDrink"] intValue];
    coctail.name = [dict objectForKey:@"strDrink"];
    coctail.category = [dict objectForKey:@"strCategory"];
    coctail.glassType = [dict objectForKey:@"strGlass"];
    coctail.alcoholType = [dict objectForKey:@"strAlcoholic"];
    coctail.recipe = [dict objectForKey:@"strInstructions"];
    coctail.picture = [NSData dataWithContentsOfURL:[NSURL URLWithString:[dict objectForKey:@"strDrinkThumb"]]];
    coctail.date = [NSDate date];
    
    NSInteger index = 1;
    NSString* ingredientKey = [NSString stringWithFormat:@"strIngredient%ld", index];
    NSString* measureKey = [NSString stringWithFormat:@"strMeasure%ld", index];
    while ([[dict objectForKey:ingredientKey] length] != 0) {
        
        [self addIngredient:[dict objectForKey:ingredientKey]
                    measure:[dict objectForKey:measureKey]
                     parent:coctail];
        
        index++;
        ingredientKey = [NSString stringWithFormat:@"strIngredient%ld", index];
        measureKey = [NSString stringWithFormat:@"strMeasure%ld", index];
    }
    
    self.currentCoctail = coctail;
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

- (BOOL) isUnique: (NSInteger) coctailId {
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity: [NSEntityDescription entityForName: @"Coctails" inManagedObjectContext: [self.fetchedCoctailsResultsController managedObjectContext]]];
    
    NSError *error = nil;
    NSUInteger count = [self.managedObjectContext countForFetchRequest: request error: &error];
    for (int i = 0; i < count; i++) {
        APITestCoctailsMO* coctail = [self.fetchedCoctailsResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (coctail.identifier == coctailId) {
            return NO;
        }
    }
    return YES;
}

- (void) addCoctail {
    
    AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
    [manager GET: @"https://www.thecocktaildb.com/api/json/v1/1/random.php"
      parameters:nil
        progress:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             NSLog(@"JSON: %@", responseObject);
             self.dict = responseObject;
             NSIndexPath* path = [NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:0] inSection:0];
             NSArray* dic = [self.dict objectForKey:@"drinks"];
             NSDictionary* dict = [dic objectAtIndex:0];
             if ([self isUnique:[[dict objectForKey:@"idDrink"] integerValue]]) {
                 [self.tableView beginUpdates];
                 [self insertCoctailInCoreData:dict];
                 [self.tableView insertRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationAutomatic];
                 [self.tableView endUpdates];
             }
             else {
                 [self addCoctail];
             }
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSLog(@"Error: %@", error);
         }];
}

/* add ingredient, measure and parent */
- (void) addIngredient: (NSString*) ingredient measure: (NSString*) measure parent: (APITestCoctailsMO*) parent {
    
    NSManagedObjectContext* context = [self.fetchedIngredientsResultsController managedObjectContext];
    IngredientsMO* ingredientValue = [NSEntityDescription insertNewObjectForEntityForName:@"Ingredients"
                                                                   inManagedObjectContext:context];
    
    ingredientValue.ingredient = ingredient;
    ingredientValue.measure = measure;
    ingredientValue.coctails = parent;
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

- (NSArray*) getAllIngredients: (APITestCoctailsMO*) parent {
    
    NSFetchedResultsController* rc = [self fetchedIngredientsResultsController];
    NSMutableArray* ingredientsList = [NSMutableArray array];
    for (int i = 0; i < rc.fetchedObjects.count; i++) {
        IngredientsMO* ingr = [rc.fetchedObjects objectAtIndex:i];
        
        if (ingr.coctails == parent) {
            [ingredientsList addObject:ingr];
        }
    }
    return ingredientsList;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController<APITestCoctailsMO *> *)fetchedCoctailsResultsController {
    if (_fetchedCoctailsResultsController != nil) {
        return _fetchedCoctailsResultsController;
    }
    
    NSFetchRequest<APITestCoctailsMO *> *fetchRequest = APITestCoctailsMO.fetchRequest;
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];

    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController<APITestCoctailsMO *> *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    _fetchedCoctailsResultsController = aFetchedResultsController;
    return _fetchedCoctailsResultsController;
}

- (NSFetchedResultsController<IngredientsMO *> *)fetchedIngredientsResultsController {
    if (_fetchedIngredientsResultsController != nil) {
        return _fetchedIngredientsResultsController;
    }
    
    NSFetchRequest<IngredientsMO *> *fetchRequest = IngredientsMO.fetchRequest;
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"ingredient" ascending:NO];
    
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController<IngredientsMO *> *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    _fetchedIngredientsResultsController = aFetchedResultsController;
    return _fetchedIngredientsResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Actions

- (IBAction)addCoctailAction:(UIBarButtonItem *)sender {
    
    [self addCoctail];
}

- (IBAction)deleteCoreDataAction:(UIBarButtonItem *)sender {
    
    NSFetchRequest *requestCount = [[NSFetchRequest alloc] init];
    [requestCount setEntity: [NSEntityDescription entityForName: @"Coctails" inManagedObjectContext: [self.fetchedCoctailsResultsController managedObjectContext]]];
    
    NSError *error = nil;
    NSUInteger count = [self.managedObjectContext countForFetchRequest: requestCount error: &error];
    
    [self.tableView beginUpdates];
    /* delete coctails */
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Coctails"];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    NSError *deleteError = nil;
    [self.coord executeRequest:delete withContext:[self.fetchedCoctailsResultsController managedObjectContext] error:&deleteError];
    
    /* delete ingredients */
    NSFetchRequest *requestIn = [[NSFetchRequest alloc] initWithEntityName:@"Ingredients"];
    NSBatchDeleteRequest *deleteIn = [[NSBatchDeleteRequest alloc] initWithFetchRequest:requestIn];
    NSError *deleteErrorIn = nil;
    [self.coord executeRequest:deleteIn withContext:[self.fetchedIngredientsResultsController managedObjectContext] error:&deleteErrorIn];
    
    /* delete rows */
    for (int i = 0; i < count; i++) {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView reloadData];
    [self.tableView endUpdates];
}

@end
