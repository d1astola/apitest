//
//  DrinkCellTableViewCell.h
//  APITest
//
//  Created by Tester on 3/16/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrinkCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (strong, nonatomic) NSDictionary* descript;

@end


