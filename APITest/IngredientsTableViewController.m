//
//  IngredientsTableViewController.m
//  APITest
//
//  Created by Tester on 3/19/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "IngredientsTableViewController.h"

@interface IngredientsTableViewController ()

@property (assign, nonatomic) NSInteger countIngredients;

@end

@implementation IngredientsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.countIngredients = 0;
    
    for (int i = 1; ; i++) {
        
        NSString* value = [self getIngredient:i];
        
        if ([value length] == 0) {
            break;
        }
        else {
            self.countIngredients++;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (NSString*) getIngredient: (NSInteger) index {
    NSString* indetifier = [NSString stringWithFormat:@"strIngredient%ld", index];
    return [self.descript objectForKey:indetifier];
}

- (NSString*) getMeasure: (NSInteger) index {
    NSString* indetifier = [NSString stringWithFormat:@"strMeasure%ld", index];
    return [self.descript objectForKey:indetifier];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.ingredientsList.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IngredientsMO* ingr = [self.ingredientsList objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", ingr.ingredient, ingr.measure];
    return cell;
}

@end
