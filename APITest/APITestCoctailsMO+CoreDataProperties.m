//
//  APITestCoctailsMO+CoreDataProperties.m
//  
//
//  Created by Tester on 3/19/18.
//
//

#import "APITestCoctailsMO+CoreDataProperties.h"

@implementation APITestCoctailsMO (CoreDataProperties)

+ (NSFetchRequest<APITestCoctailsMO *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Coctails"];
}

@dynamic identifier;
@dynamic name;
@dynamic category;
@dynamic alcoholType;
@dynamic glassType;
@dynamic recipe;
@dynamic ingredients;

@end
