//
//  DetailedCoctailViewController.m
//  APITest
//
//  Created by Tester on 3/16/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "DetailedCoctailViewController.h"
#import "IngredientsTableViewController.h"

@interface DetailedCoctailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *coctailImage;
@property (weak, nonatomic) IBOutlet UILabel *coctailName;
@property (weak, nonatomic) IBOutlet UITextView *repiceTextView;
@property (weak, nonatomic) IBOutlet UILabel *alcoholicLabel;
@property (weak, nonatomic) IBOutlet UILabel *glassTypeLabel;

- (IBAction)showIngredientsAction:(UIButton *)sender;

@end

@implementation DetailedCoctailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.coctailName.text = self.coctail.name;
    
    self.coctailImage.image = [UIImage imageWithData:self.coctail.picture];
    
    self.repiceTextView.text = self.coctail.recipe;
    
    self.alcoholicLabel.text = self.coctail.alcoholType;
    
    self.glassTypeLabel.text = self.coctail.glassType;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    IngredientsTableViewController* itvc = [[IngredientsTableViewController alloc] init];
    itvc = segue.destinationViewController;
    itvc.ingredientsList = self.ingredientsList;
}

#pragma mark - Actions

- (IBAction)showIngredientsAction:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"showIngredients" sender:self];
}
@end
