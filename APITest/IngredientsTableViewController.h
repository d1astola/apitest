//
//  IngredientsTableViewController.h
//  APITest
//
//  Created by Tester on 3/19/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IngredientsMO+CoreDataClass.h"

@interface IngredientsTableViewController : UIViewController

@property (strong, nonatomic) NSDictionary* descript;
@property (strong, nonatomic) NSArray* ingredientsList;

@end
