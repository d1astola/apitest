//
//  APITestCoctailsMO+CoreDataClass.h
//  
//
//  Created by Tester on 3/19/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IngredientsMO;

NS_ASSUME_NONNULL_BEGIN

@interface APITestCoctailsMO : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "APITestCoctailsMO+CoreDataProperties.h"
