//
//  DetailedCoctailViewController.h
//  APITest
//
//  Created by Tester on 3/16/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model+CoreDataModel.h"

@interface DetailedCoctailViewController : UIViewController

@property (strong, nonatomic) APITestCoctailsMO* coctail;
@property (strong, nonatomic) NSArray* ingredientsList;

@end
