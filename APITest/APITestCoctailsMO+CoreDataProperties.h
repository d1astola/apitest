//
//  APITestCoctailsMO+CoreDataProperties.h
//  
//
//  Created by Tester on 3/19/18.
//
//

#import ".APITestCoctailsMO+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface APITestCoctailsMO (CoreDataProperties)

+ (NSFetchRequest<APITestCoctailsMO *> *)fetchRequest;

@property (nonatomic) int16_t identifier;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *category;
@property (nullable, nonatomic, copy) NSString *alcoholType;
@property (nullable, nonatomic, copy) NSString *glassType;
@property (nullable, nonatomic, copy) NSString *recipe;
@property (nullable, nonatomic, retain) IngredientsMO *ingredients;

@end

NS_ASSUME_NONNULL_END
