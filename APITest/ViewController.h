//
//  ViewController.h
//  ReadTwitterRSS
//
//  Created by Tester on 3/15/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailedCoctailViewController.h"
#import "Model+CoreDataModel.h"
#import <AFNetworking.h>
#import <CoreData/CoreData.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSDictionary* selectedDictionary;

@property (strong, nonatomic) NSFetchedResultsController<APITestCoctailsMO *> *fetchedCoctailsResultsController;
@property (strong, nonatomic) NSFetchedResultsController<IngredientsMO *> *fetchedIngredientsResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSPersistentStoreCoordinator* coord;
@property (assign, nonatomic) BOOL coreDataIsLoading;

@end

